#ifndef SFRAME_USER_MatrixElementAnalysis_H
#define SFRAME_USER_MatrixElementAnalysis_H

#include "TLorentzVector.h"

/**
 * @brief Performs an analysis on the matrix element part of the truth record.
 *
 * Set the top quark, anti-top, and leptons and the value for kappa in the
 * constructor.
 */
class MatrixElementAnalysis {
public:
    ///Construct the matrix element analysis.
    MatrixElementAnalysis();

    ///Destroy the matrix element analysis.
    ~MatrixElementAnalysis();

    ///Get Cos(Theta1) in the beam basis.
    inline double getBeamTheta1() const {
        return a;
    }

    ///Get Cos(Theta2) in the beam basis.
    inline double getBeamTheta2() const {
        return b;
    }

    ///Get D in the beam basis.
    inline double getBeamD() const {
        return beamD;
    }

    ///Get the weight of the event (based on kappa) in the beam basis.
    inline double getBeamWeight() const {
        //cout << "beam a " << a << endl;
        //cout << "beam b " << b << endl;
        return reweight(a, b);
    }

    inline double getDWeight() const {
        return reweightD(helicityD);
    }

    ///Get Cos(Theta1) in the helicity basis.
    inline double getHelicityTheta1() const {
        return c;
    }

    ///Get Cos(Theta2) in the helicity basis.
    inline double getHelicityTheta2() const {
        return d;
    }

    ///Return D in the helicity basis.
    inline double getHelicityD() const {
        return helicityD;
    }

    ///Return the weight of this event in the helicity basis.
    inline double getHelicityWeight() const {
        //std::cout << "c " << c << " d " << d << std::endl;
        return reweight(c, d);
    }

    ///Get Cos(Theta1) in the off diagonal basis.
    inline double getOffDiagonalTheta1() const {
        return e;
    }

    ///Get Cos(Theta2) in the off diagonal basis.
    inline double getOffDiagonalTheta2() const {
        return f;
    }

    ///Return D in the off diagonal basis.
    inline double getOffDiagonalD() const {
        return offDiagD;
    }

    ///Return the weight of this event in the off diagonal basis.
    inline double getOffDiagonalWeight() const {
        return reweight(e, f);
    }

    ///Get Cos(Theta1) in the off diagonal basis.
    inline double getLHCOptimisedTheta1() const {
        return g;
    };

    ///Get Cos(Theta2) in the off diagonal basis.
    inline double getLHCOptimisedTheta2() const {
        return h;
    }

    ///Return D in the off diagonal basis.
    inline double getLHCOptimisedD() const {
        return optD;
    }

    ///Return the weight of this event in the off diagonal basis.
    inline double getLHCOptimisedWeight() const {
        return reweight(g, h);
    }

    ///Return Mahlon and Parke cos(theta) measured in ZMF frame
    inline double getMandPCosTheta() const {
        return mp_cos_theta;
    }

    ///Return S Ratio
    inline double getSRatio() const {
        return S_ratio;
    }

    ///Code to do all the calculations.
    void calculate(double, TLorentzVector, TLorentzVector, TLorentzVector, TLorentzVector);

    ///Calculate the W polarisation angle.
    double wPolarisation(TLorentzVector top, TLorentzVector w, TLorentzVector lepton);

private:
    ///Calculate the weight of the event, in this basis.
    double reweight(double, double) const;

    double reweightD(double cosphi) const;

    ///Value of D in the beam line basis. Filled by calculate.
    double beamD;

    ///Value of D in the helicity basis. Filled by calculate.
    double helicityD;

    ///Value of D in the off diagonal basis. Filled by calculate.
    double offDiagD;

    ///Value of D in the LHC optimised basis
    double optD;

    double a, b, c, d, e, f, g, h;

    ///Mahlon and Parke cos(theta) measured in ZMF
    double mp_cos_theta;

    ///S Ratio
    double S_ratio;

    ///C for reweighting.
    double inputc;
};

#endif  /* _MATRIXELEMENTANALYSIS_H */
