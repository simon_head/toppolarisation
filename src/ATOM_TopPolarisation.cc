// -*- C++ -*-
#include "Atom/AtomAnalysis.hh"
#include "Atom/Detectors/Atlas.hh"
#include "Atom/Detectors/Atlas2011.hh"

#include "Atom/VariableCut.hh"
#include "Atom/EfficiencyHelper.hh"
#include "Atom/XSecHelper.hh"

#include "TLorentzVector.h"
#include "TVector3.h"
#include "TVector2.h"
#include "MatrixElementAnalysis.h"

namespace Atom {

  double _transMass(double ptLep, double phiLep, double met, double phiMet) {
    return sqrt(2.0*ptLep*met*(1 - cos(phiLep-phiMet)));
  }

  vector<double> BaumgartTweedie(TLorentzVector t, TLorentzVector tbar, TLorentzVector lp, TLorentzVector lm) {
      const double pt = t.Pt() + tbar.Pt();

      //boost to com
      TLorentzVector ttbar = t + tbar;
      TVector3 boost_to_ttbar = -1. * ttbar.BoostVector();

      t.Boost(boost_to_ttbar);
      tbar.Boost(boost_to_ttbar);
      lp.Boost(boost_to_ttbar);
      lm.Boost(boost_to_ttbar);

      //boost from ZMF to top frame
      TVector3 boost_to_top = -1. * t.BoostVector();
      lp.Boost(boost_to_top);

      //boost from ZMF to tbar frame
      TVector3 boost_to_tbar = -1. * tbar.BoostVector();
      lm.Boost(boost_to_tbar);

      TVector3 beamZ(0, 0, 1);

      TVector3 newZ = t.Vect().Unit();
      TVector3 newY = t.Vect().Unit().Cross(beamZ).Unit();
      TVector3 newX = newY.Cross(newZ).Unit();

      TRotation m;
      m.RotateAxes(newX, newY, newZ);
      m = m.Inverse();
      lp.Transform(m);
      lm.Transform(m);

      vector<double> ret;

      ret.push_back(TVector2::Phi_mpi_pi(lp.Phi() - lm.Phi()));
      ret.push_back(TVector2::Phi_mpi_pi(lp.Phi() + lm.Phi()));

      return ret;
  }


    class ATOM_TopPolarisation : public AtomAnalysis {
    public:

        /// @name Constructors etc.
        //@{

        /// Constructor
        ATOM_TopPolarisation()
            : AtomAnalysis("ATOM_TopPolarisation"),
              _atlas(getAtlas2011(_bEff, _bSmear)) {
            /// @todo Set whether your finalize method needs the generator cross section
            setNeedsCrossSection(true);
        }

        //@}


#define ATLAS_LEPT_JET_SEP  0.4
#define ATLAS_TIGHT_E_EFF   0.75

#define ATLAS_TRACK_PTMIN  3.0
#define ATLAS_TRACK_SEP    0.4
#define ATLAS_ISO_TR_PTMIN 12.0

    public:

        /// @name Analysis methods
        //@{

        /// Book histograms and initialise projections before the run
        void initLocal() {
            FinalState base = _atlas.finalState();
            RangeSelector ele_range = RangeSelector(RangeSelector::TRANSVERSE_MOMENTUM, 25.0, 14000.) & RangeSelector(RangeSelector::PSEUDO_RAPIDITY, -2.5, 2.5);
            IsoElectron ele_pre = _atlas.electronBase(ele_range, 0.2, 0.15, 0.0);
            FastSimFinalState<IsoElectron> ele_smear = _atlas.mediumElectron(ele_pre);
            addProjection(ele_smear, "mediumElectron");
            FastSimFinalState<IsoElectron> ele_tight_smear = _atlas.tightElectron(ele_pre);
            addProjection(ele_tight_smear, "tightElectron");
            RangeSelector mu_range = RangeSelector(RangeSelector::TRANSVERSE_MOMENTUM, 25.0, 14000.) & RangeSelector(RangeSelector::PSEUDO_RAPIDITY, -2.5, 2.5);
            IsoMuon mu_pre = _atlas.muonBase(mu_range, 0.2, 0.15, 0.0);
            FastSimFinalState<IsoMuon> mu_smear = _atlas.combinedMuon(mu_pre);
            addProjection(mu_smear, "combinedMuons");
            JetFinalState jets_pre = _atlas.jetBase(base, 0.4, _atlas.topoJetRange());
            FastSimFinalState<JetFinalState> jets = _atlas.topoJet(jets_pre);
            addProjection(jets, "topoclusterJets");
            NearIsoParticle jets_clean = _atlas.removeOverlap(jets, ele_smear, _atlas.jetElectronSeparation());
            addProjection(jets_clean, "Jets");
            NearIsoParticle  ele_clean = _atlas.removeOverlap(ele_smear, jets_clean, _atlas.leptonJetSeparation());
            addProjection(ele_clean, "IsoElectrons");
            NearIsoParticle  ele_tight_clean = _atlas.removeOverlap(ele_tight_smear, jets_clean, _atlas.leptonJetSeparation());
            addProjection(ele_tight_clean, "IsoTightElectrons");
            NearIsoParticle   mu_clean = _atlas.removeOverlap(mu_smear, jets_clean, _atlas.leptonJetSeparation());
            addProjection(mu_clean, "IsoMuons");
            MergedFinalState leptons = _atlas.leptonsMerged(mu_clean, ele_clean);
            addProjection(leptons, "Leptons");
            double eff = 0.70;
            double rejection = 100;
            TagParameterization Bbtag = _atlas.simple_Btag(eff, rejection);
            RangeSelector bjrange = _atlas.bJetRange() & RangeSelector(RangeSelector::TRANSVERSE_MOMENTUM, 25.0, 14000.0) & RangeSelector(RangeSelector::PSEUDO_RAPIDITY, -2.5, 2.5);
            RandomJetTagger bjets(jets, Bbtag, bjrange);
            addProjection(bjets, "BJets");
            MissingMomentum met = _atlas.met(base);
            addProjection(met, "MissingEt");
            // Projection to find neutrinos
            IdentifiedFinalState nu_id;
            nu_id.acceptNeutrinos();
            addProjection(nu_id, "neutrinos");
            // don't think we need tracks but included for reference for now
            RangeSelector track_range = RangeSelector(RangeSelector::TRANSVERSE_MOMENTUM, 12.0, 14000.) & RangeSelector(RangeSelector::PSEUDO_RAPIDITY, -2.5, 2.5);
            FastSimFinalState<NearIsoParticle> tracks = _atlas.track(leptons, track_range, 0.4);
            addProjection(tracks, "IsoTracks");

            /// @todo Book histograms here, e.g.:
            _histos["DPHI_LPLM"]   = bookHisto1D("dphi_lplm", 20, 0, M_PI, "", "dphi(lp lm)", "Relative Occurence");
            _histos["COSTHETACOSTHETA"]   = bookHisto1D("costhetacostheta", 20, -1, 1, "", "cos theta+ cos theta-", "Relative Occurence");
            _histos["COSTHETACOSTHETA_UWER"]   = bookHisto1D("costhetacostheta_uwer", 20, -1, 1, "", "cos theta+ cos theta- Uwer basis", "Relative Occurence");
            _histos["BT_DIFF"]   = bookHisto1D("bt_diff", 20, -M_PI, M_PI, "", "dphi BT", "Relative Occurence");
            _histos["BT_SUM"]   = bookHisto1D("bt_sum", 20, -M_PI, M_PI, "", "sum phi BT", "Relative Occurence");
            _histos["LEADINGJ_PT"]   = bookHisto1D("leadingjetpt", 20, 0, 300, "", "leading jet pt", "Relative Occurence");
            _histos["LEADINGT_MASS"]   = bookHisto1D("top1mass", 20, 0, 300, "", "top1 mass", "Relative Occurence");
            _histos["SUBLEADINGT_MASS"]   = bookHisto1D("top2mass", 20, 0, 300, "", "top2 mass", "Relative Occurence");
        }


        /// Perform the per-event analysis
        bool analyzeLocal(const Event& event, const double weight) {
            _top1c.clear();
            _top2c.clear();

            const Particles jets = applyProjection<NearIsoParticle>(event, "Jets").particlesByPt();
            const Particles lepts = applyProjection<MergedFinalState>(event, "Leptons").particlesByPt();
            const Particles tight_e = applyProjection<NearIsoParticle>(event, "IsoTightElectrons").particlesByPt();
            const MissingMomentum pmet = applyProjection<MissingMomentum>(event, "MissingEt");
            const Particles bjets = applyProjection<RandomJetTagger>(event, "BJets").getTaggedJets();
            const Particles neutrinos = applyProjection<IdentifiedFinalState>(event, "neutrinos").particlesByPt();
            const FourMomentum met = pmet.missingEt();

            if (lepts.size() != 2) {
                vetoEvent;
            }
            // two OS leptons
            if (lepts[0].pdgId()*lepts[1].pdgId() > 0) {
                vetoEvent;
            }

            if (bjets.size() != 2) {
                vetoEvent;
            }

            if (met.pT() < 60.) {
                vetoEvent;
            }

            if( (lepts[0].momentum()+bjets[0].momentum()).mass() + (lepts[1].momentum()+bjets[1].momentum()).mass() < (lepts[0].momentum()+bjets[1].momentum()).mass() + (lepts[1].momentum()+bjets[0].momentum()).mass() ) {
              _top1 = lepts[0].momentum() + bjets[0].momentum();
              _top1c.push_back(lepts[0].momentum()); _top1c.push_back(bjets[0].momentum());
              _top2 = lepts[1].momentum() + bjets[1].momentum();
              _top2c.push_back(lepts[1].momentum()); _top2c.push_back(bjets[1].momentum());
            }
            else {
              _top1 = lepts[0].momentum() + bjets[1].momentum();
              _top1c.push_back(lepts[0].momentum()); _top1c.push_back(bjets[1].momentum());
              _top2 = lepts[1].momentum() + bjets[0].momentum();
              _top2c.push_back(lepts[1].momentum()); _top2c.push_back(bjets[0].momentum());
            }
            /// At this point we have tops before adding neutrinos so still "realistic"

            if( abs((_top1+neutrinos[0].momentum()).mass() - 172.5) + abs((_top2+neutrinos[1].momentum()).mass() - 172.5) < abs((_top1+neutrinos[1].momentum()).mass() - 172.5) + abs((_top2+neutrinos[0].momentum()).mass() - 172.5) ) {
              _top1 = _top1 + neutrinos[0].momentum();
              _top1c.push_back(neutrinos[0].momentum());
              _top2 = _top2 + neutrinos[1].momentum();
              _top2c.push_back(neutrinos[1].momentum());
            }
            else {
              _top1 = _top1 + neutrinos[1].momentum();
              _top1c.push_back(neutrinos[1].momentum());
              _top2 = _top2 + neutrinos[0].momentum();
              _top2c.push_back(neutrinos[0].momentum());
            }
//            cout << _top1.mass() << "         " << _top2.mass() << endl;

            MatrixElementAnalysis mea;
            TLorentzVector t(_top1.px(), _top1.py(), _top1.pz(), _top1.E());
            TLorentzVector lp(_top1c[0].px(), _top1c[0].py(), _top1c[0].pz(), _top1c[0].E());
            TLorentzVector tbar(_top2.px(), _top2.py(), _top2.pz(), _top2.E());
            TLorentzVector lm(_top2c[0].px(), _top2c[0].py(), _top2c[0].pz(), _top2c[0].E());
            mea.calculate(1., t, tbar, lp, lm);

            _histos["LEADINGT_MASS"]->fill(_top1.mass(),weight);
            _histos["LEADINGJ_PT"]->fill(jets[0].pT(),weight);
            _histos["SUBLEADINGT_MASS"]->fill(_top2.mass(),weight);

//            cout << fabs(lp.DeltaPhi(lm)) << endl;
            _histos["DPHI_LPLM"]->fill(fabs(lp.DeltaPhi(lm)),weight);
            _histos["COSTHETACOSTHETA"]->fill(mea.getHelicityTheta1() * mea.getHelicityTheta2(),weight);
            _histos["COSTHETACOSTHETA_UWER"]->fill(mea.getLHCOptimisedTheta1() * mea.getLHCOptimisedTheta2(),weight);

            vector<double> bts = BaumgartTweedie(t, tbar, lp, lm);
            _histos["BT_DIFF"]->fill(bts[0],weight);
            _histos["BT_SUM"]->fill(bts[1],weight);

            return true;
        }


        /// Normalise histograms etc., after the run
        void finalizeLocal() {
            /// @todo Change the luminosity here
            _luminosity = std::make_pair(35.3 / picobarn, 1.2 / 35.3);
            double norm = crossSection() * _luminosity.first / sumOfWeights();
            /// @todo Normalise, scale and otherwise manipulate histograms here
        }

        //@}


    private:

        ATLASProjections _atlas;


    private:

        /// @name Histograms
        //@{

        std::map<std::string, Histo1DPtr> _histos;
        vector<FourMomentum> _top1c, _top2c;
        FourMomentum _top1,_top2;

        //@}


    };



    // This global object acts as a hook for the plugin system
    AtomPlugin(ATOM_TopPolarisation)


}
