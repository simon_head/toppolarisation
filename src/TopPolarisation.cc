// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/PromptFinalState.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/FastJets.hh"
#include "TLorentzVector.h"
#include "TVector3.h"
#include "TVector2.h"
#include "MatrixElementAnalysis.h"

namespace Rivet {

  double _transMass(double ptLep, double phiLep, double met, double phiMet) {
    return sqrt(2.0*ptLep*met*(1 - cos(phiLep-phiMet)));
  }

  vector<double> BaumgartTweedie(TLorentzVector t, TLorentzVector tbar, TLorentzVector lp, TLorentzVector lm) {
      const double pt = t.Pt() + tbar.Pt();

      //boost to com
      TLorentzVector ttbar = t + tbar;
      TVector3 boost_to_ttbar = -1. * ttbar.BoostVector();

      t.Boost(boost_to_ttbar);
      tbar.Boost(boost_to_ttbar);
      lp.Boost(boost_to_ttbar);
      lm.Boost(boost_to_ttbar);

      //boost from ZMF to top frame
      TVector3 boost_to_top = -1. * t.BoostVector();
      lp.Boost(boost_to_top);

      //boost from ZMF to tbar frame
      TVector3 boost_to_tbar = -1. * tbar.BoostVector();
      lm.Boost(boost_to_tbar);

      TVector3 beamZ(0, 0, 1);

      TVector3 newZ = t.Vect().Unit();
      TVector3 newY = t.Vect().Unit().Cross(beamZ).Unit();
      TVector3 newX = newY.Cross(newZ).Unit();

      TRotation m;
      m.RotateAxes(newX, newY, newZ);
      m = m.Inverse();
      lp.Transform(m);
      lm.Transform(m);

      vector<double> ret;

      ret.push_back(TVector2::Phi_mpi_pi(lp.Phi() - lm.Phi()));
      ret.push_back(TVector2::Phi_mpi_pi(lp.Phi() + lm.Phi()));

      return ret;
  }

  /// @brief top polarisation
  class TopPolarisation : public Analysis {
  public:

    /// Default constructor
    TopPolarisation() : Analysis("TopPolarisation")
    {    }


    /// @name Analysis methods
    //@{

    void init() {

      // Eta ranges
      Cut eta_full = Cuts::abseta < 5.0 && Cuts::pT > 500.0*MeV;
      Cut eta_lep = Cuts::abseta < 2.5;

      // All final state particles
      FinalState fs(eta_full);

      // Get photons to dress leptons
      IdentifiedFinalState photons(fs);
      photons.acceptIdPair(PID::PHOTON);

      // Projection to find the electrons
      IdentifiedFinalState el_id(fs);
      el_id.acceptIdPair(PID::ELECTRON);
      PromptFinalState electrons(el_id);
      electrons.acceptTauDecays(true);
      addProjection(electrons, "electrons");
      DressedLeptons dressedelectrons(photons, electrons, 0.1, eta_lep && Cuts::pT > 25*GeV, true, true);
      addProjection(dressedelectrons, "dressedelectrons");
      DressedLeptons ewdressedelectrons(photons, electrons, 0.1, eta_full, true, true);
      addProjection(ewdressedelectrons, "ewdressedelectrons");

      // Projection to find the muons
      IdentifiedFinalState mu_id(fs);
      mu_id.acceptIdPair(PID::MUON);
      PromptFinalState muons(mu_id);
      muons.acceptTauDecays(true);
      addProjection(muons, "muons");
      vector<pair<double, double> > eta_muon;
      DressedLeptons dressedmuons(photons, muons, 0.1, eta_lep && Cuts::pT >= 25*GeV, true, true);
      addProjection(dressedmuons, "dressedmuons");
      DressedLeptons ewdressedmuons(photons, muons, 0.1, eta_full, true, true);
      addProjection(ewdressedmuons, "ewdressedmuons");

      // Projection to find neutrinos and produce MET
      IdentifiedFinalState nu_id;
      nu_id.acceptNeutrinos();
      PromptFinalState neutrinos(nu_id);
      neutrinos.acceptTauDecays(true);
      addProjection(neutrinos, "neutrinos");

      // Jet clustering.
      VetoedFinalState vfs;
      vfs.addVetoOnThisFinalState(ewdressedelectrons);
      vfs.addVetoOnThisFinalState(ewdressedmuons);
      vfs.addVetoOnThisFinalState(neutrinos);
      FastJets jets(vfs, FastJets::ANTIKT, 0.4);
      jets.useInvisibles();
      addProjection(jets, "jets");

      _histos["DPHI_LPLM"]   = bookHisto1D("dphi_lplm", 20, 0, M_PI, "", "dphi(lp lm)", "Relative Occurence");
      _histos["COSTHETACOSTHETA"]   = bookHisto1D("costhetacostheta", 20, -1, 1, "", "cos theta+ cos theta-", "Relative Occurence");
      _histos["COSTHETACOSTHETA_UWER"]   = bookHisto1D("costhetacostheta_uwer", 20, -1, 1, "", "cos theta+ cos theta- Uwer basis", "Relative Occurence");
      _histos["BT_DIFF"]   = bookHisto1D("bt_diff", 20, -M_PI, M_PI, "", "dphi BT", "Relative Occurence");
      _histos["BT_SUM"]   = bookHisto1D("bt_sum", 20, -M_PI, M_PI, "", "sum phi BT", "Relative Occurence");
      _histos["LEADINGJ_PT"]   = bookHisto1D("leadingjetpt", 20, 0, 300, "", "leading jet pt", "Relative Occurence");
      _histos["LEADINGT_MASS"]   = bookHisto1D("top1mass", 20, 0, 300, "", "top1 mass", "Relative Occurence");
      _histos["SUBLEADINGT_MASS"]   = bookHisto1D("top2mass", 20, 0, 300, "", "top2 mass", "Relative Occurence");

    }


    void analyze(const Event& event) {

      const double weight = event.weight();
      _dressedleptons.clear();
      _bjets.clear();
      _top1c.clear();
      _top2c.clear();

      // Get the selected objects, using the projections.
      _dressedelectrons = sortByPt(applyProjection<DressedLeptons>(event, "dressedelectrons").dressedLeptons());

      _dressedmuons = sortByPt(applyProjection<DressedLeptons>(event, "dressedmuons").dressedLeptons());

      _neutrinos = applyProjection<PromptFinalState>(event, "neutrinos").particlesByPt();

      _jets = applyProjection<FastJets>(event, "jets").jetsByPt(Cuts::pT > 25*GeV && Cuts::abseta < 2.5);


      // Calculate the missing ET, using the prompt neutrinos only (really?)
      /// @todo Why not use MissingMomentum?
      FourMomentum pmet;
      foreach (const Particle& p, _neutrinos) pmet += p.momentum();
      _met_et = pmet.pT();
      _met_phi = pmet.phi();

      // Check overlap of jets/leptons.
      unsigned int i,j;
      _jet_ntag = 0;
      _overlap = false;
      for (i = 0; i < _jets.size(); i++) {
        const Jet& jet = _jets[i];
        // If dR(el,jet) < 0.4 skip the event
        foreach (const DressedLepton& el, _dressedelectrons) {
          if (deltaR(jet, el) < 0.4) _overlap = true;
        }
        // If dR(mu,jet) < 0.4 skip the event
        foreach (const DressedLepton& mu, _dressedmuons) {
          if (deltaR(jet, mu) < 0.4) _overlap = true;
        }
        // If dR(jet,jet) < 0.5 skip the event
        for (j = 0; j < _jets.size(); j++) {
          const Jet& jet2 = _jets[j];
          if (i == j) continue; // skip the diagonal
          if (deltaR(jet, jet2) < 0.5) _overlap = true;
        }
        // Count the number of b-tags
        if (!jet.bTags().empty()) { _jet_ntag += 1; _bjets.push_back(jet); }
      }

      // no overlap
      if(_overlap) vetoEvent;
      // need two leptons
      if(_dressedelectrons.size() + _dressedmuons.size() != 2 || _neutrinos.size() < 2) vetoEvent;
      foreach(const DressedLepton& el, _dressedelectrons) {
        _dressedleptons.push_back(el);
      }
      foreach(const DressedLepton& mu, _dressedmuons) {
        _dressedleptons.push_back(mu);
      }
      // want two jets with b tags
      if(_jet_ntag != 2) vetoEvent;
      // want et_miss > 60 GeV
      if(_met_et < 60.) vetoEvent;

      bool top1p = false;

      if( (_dressedleptons[0].momentum()+_bjets[0].momentum()).mass() + (_dressedleptons[1].momentum()+_bjets[1].momentum()).mass() < (_dressedleptons[0].momentum()+_bjets[1].momentum()).mass() + (_dressedleptons[1].momentum()+_bjets[0].momentum()).mass() ) {
        if(_dressedleptons[0].charge() > 0) top1p = true;
        _top1 = _dressedleptons[0].momentum() + _bjets[0].momentum();
        _top1c.push_back(_dressedleptons[0].momentum()); _top1c.push_back(_bjets[0].momentum());
        _top2 = _dressedleptons[1].momentum() + _bjets[1].momentum();
        _top2c.push_back(_dressedleptons[1].momentum()); _top2c.push_back(_bjets[1].momentum());
      }
      else {
        _top1 = _dressedleptons[0].momentum() + _bjets[1].momentum();
        _top1c.push_back(_dressedleptons[0].momentum()); _top1c.push_back(_bjets[1].momentum());
        _top2 = _dressedleptons[1].momentum() + _bjets[0].momentum();
        _top2c.push_back(_dressedleptons[1].momentum()); _top2c.push_back(_bjets[0].momentum());
      }
      /// At this point we have tops before adding neutrinos so still "realistic"

      if( abs((_top1+_neutrinos[0].momentum()).mass() - 172.5) + abs((_top2+_neutrinos[1].momentum()).mass() - 172.5) < abs((_top1+_neutrinos[1].momentum()).mass() - 172.5) + abs((_top2+_neutrinos[0].momentum()).mass() - 172.5) ) {
        _top1 = _top1 + _neutrinos[0].momentum();
        _top1c.push_back(_neutrinos[0].momentum());
        _top2 = _top2 + _neutrinos[1].momentum();
        _top2c.push_back(_neutrinos[1].momentum());
      }
      else {
        _top1 = _top1 + _neutrinos[1].momentum();
        _top1c.push_back(_neutrinos[1].momentum());
        _top2 = _top2 + _neutrinos[0].momentum();
        _top2c.push_back(_neutrinos[0].momentum());
      }

//      cout << _top1.mass() << "         " << _top2.mass() << endl;

      MatrixElementAnalysis mea;
      TLorentzVector t(_top1.px(), _top1.py(), _top1.pz(), _top1.E());
      TLorentzVector lp(_top1c[0].px(), _top1c[0].py(), _top1c[0].pz(), _top1c[0].E());
      TLorentzVector tbar(_top2.px(), _top2.py(), _top2.pz(), _top2.E());
      TLorentzVector lm(_top2c[0].px(), _top2c[0].py(), _top2c[0].pz(), _top2c[0].E());
      mea.calculate(1., t, tbar, lp, lm);

      _histos["LEADINGT_MASS"]->fill(_top1.mass(),weight);
      _histos["LEADINGJ_PT"]->fill(_jets[0].pT(),weight);
      _histos["SUBLEADINGT_MASS"]->fill(_top2.mass(),weight);

      _histos["DPHI_LPLM"]->fill(fabs(lp.DeltaPhi(lm)),weight);
      _histos["COSTHETACOSTHETA"]->fill(mea.getHelicityTheta1() * mea.getHelicityTheta2(),weight);
      _histos["COSTHETACOSTHETA_UWER"]->fill(mea.getLHCOptimisedTheta1() * mea.getLHCOptimisedTheta2(),weight);

      vector<double> bts = BaumgartTweedie(t, tbar, lp, lm);
      _histos["BT_DIFF"]->fill(bts[0],weight);
      _histos["BT_SUM"]->fill(bts[1],weight);

    }

    void finalize() {
      /// @todo Normalise!
    }

    //@}

  private:
    /// @name Objects that are used by the event selection decisions
    //@{
    vector<DressedLepton> _dressedelectrons;
    vector<DressedLepton> _dressedmuons;
    vector<DressedLepton> _dressedleptons;
    Particles _neutrinos;
    vector<FourMomentum> _top1c, _top2c;
    FourMomentum _top1,_top2;
    Jets _jets;
    Jets _bjets;
    unsigned int _jet_ntag;
    /// @todo Why not store the whole MET FourMomentum?
    double _met_et, _met_phi;
    bool _overlap;

    std::map<std::string, Histo1DPtr> _histos;
  };

  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(TopPolarisation);

}
