#include "MatrixElementAnalysis.h"
#include "TLorentzVector.h"

#include <cmath>
#include <iostream>

MatrixElementAnalysis::MatrixElementAnalysis() :
    beamD(0.),
    helicityD(0.),
    offDiagD(0.),
    optD(0.),
    a(0.),
    b(0.),
    c(0.),
    d(0.),
    e(0.),
    f(0.),
    g(0.),
    h(0.),
    mp_cos_theta(0.),
    S_ratio(0.),
    inputc(0.) {
}

/**
 * Does nothing.
 */
MatrixElementAnalysis::~MatrixElementAnalysis() {
}

/**
 * Calculates the weight of this event based on 1 + k*cos(theta1)cos(theta2).
 *
 * @param theta1 Theta1 in the correct basis.
 * @param theta2 Theta2 in the correct basis.
 *
 * @return Event weight.
 */
double MatrixElementAnalysis::reweight(double theta1, double theta2) const {
    //double val = 0.25 * (1. - inputc * theta1 * theta2);

    double val = 1. - inputc * theta1 * theta2;

    if (std::isnan(val))
        return 0.;

    return val;
}

/**
 * In this case inputc is really D.
 */
double MatrixElementAnalysis::reweightD(double cosphi) const {
    double val = 1. - inputc * cosphi;

    if (std::isnan(val))
        return 0.;

    return val;
}

/**
 * Run once per event.
 *
 * @param ic Input c value (if we want to use the reweighting, otherwise use 0).
 * @param t LorentzVector of the top quark.
 * @param tbar LorentzVector of the anti top quark.
 * @param lp LorentzVector of the lepton (associated with top - positive charge)
 * @param lm LorentzVector of the lepton (associated with anti top - negative charge).
 */
void MatrixElementAnalysis::calculate(double ic, TLorentzVector t, TLorentzVector tbar, TLorentzVector lp, TLorentzVector lm) {
    inputc = ic;

    TLorentzVector ttbar = t + tbar;

    TVector3 boost_to_ttbar = -1. * ttbar.BoostVector();

    t.Boost(boost_to_ttbar);
    tbar.Boost(boost_to_ttbar);
    lp.Boost(boost_to_ttbar);
    lm.Boost(boost_to_ttbar);

    mp_cos_theta = lm.Vect().Unit().Dot(lp.Vect().Unit());

    const double top_m = 172500.;
    double top_m_2 = top_m * top_m;
    S_ratio = ( (top_m_2) * ( ( t.Dot(lp)*t.Dot(lm) ) + ( tbar.Dot(lp)*tbar.Dot(lm)) - ( top_m_2*lp.Dot(lm)))) / ( t.Dot(lp)*tbar.Dot(lm)*t.Dot(tbar) );

    TVector3 plus_z = TVector3(0, 0, 1);
    TVector3 boost_to_top = -1. * t.BoostVector();
	// boost from ZMF to top frame
    lp.Boost(boost_to_top);

    a = lp.Vect().Unit().Dot(plus_z);

    //a = cos(lp.Vect().Unit().Angle(plus_z.Unit()));
    //cout << "a is " << a << endl;
    //cout << "     " << lp.Vect().Unit().Dot(plus_z) << endl;

    TVector3 boost_to_tbar = -1. * tbar.BoostVector();
	// boost from ZMF to tbar frame
    lm.Boost(boost_to_tbar);
    b = lm.Vect().Unit().Dot(plus_z);

    //std::cout << "b1 " << b << std::endl;

    //std::cout << "b2 " << cos(lm.Vect().Unit().Angle(plus_z.Unit())) << std::endl;
    //cout << "b is " << b << endl;
    //cout << "     " << lm.Vect().Unit().Dot(plus_z) << endl;

    //These two methods of calculating cos theta give the same answer
    //m_logger << DEBUG << "a'= " << lp.Vect().Unit() * plus_z.Unit() << SLogger::endmsg;
    //m_logger << DEBUG << "b'= " << lm.Vect().Unit() * plus_z.Unit() << SLogger::endmsg;
    //m_logger << DEBUG << "a=  " << a << " b=" << b << SLogger::endmsg;

    //m_logger << DEBUG << "Doing it olde skool!" << SLogger::endmsg;

    //m_logger << DEBUG << "a= " << Calc_costhe1(tme,ttbarme,lme) << SLogger::endmsg;
    //m_logger << DEBUG << "b= " << Calc_costhe2(tbme,ttbarme,lmme) << SLogger::endmsg;

    //m_logger << DEBUG << SLogger::endmsg;

    //beamD = cos(lm.Vect().Unit().Angle(lp.Vect().Unit()));
    beamD = lm.Vect().Unit().Dot(lp.Vect().Unit());

    //HELICITY BASIS
    //ttbar = t + tbar;
    //boost_to_ttbar = -1 * ttbar.BoostVector();

    //t.Boost(boost_to_ttbar);
    c = lp.Vect().Unit().Dot(t.Vect().Unit());

    //tbar.Boost(boost_to_ttbar);
    d = lm.Vect().Unit().Dot(-1 * t.Vect().Unit()); //tbar.Vect().Unit()));

    //std::cout << "d here " << d << std::endl;

    //helicityD = cos(lm.Vect().Angle(lp.Vect()));
    helicityD = lm.Vect().Unit().Dot(lp.Vect().Unit());

    //OFF DIAGONAL BASIS
    double gamma = tbar.E() / tbar.M();
    double pdotk = plus_z.Dot(t.Vect().Unit());

    double fact1 = (1 - gamma) * (pdotk);
    double fact2 = sqrt(1 - (pdotk)*(pdotk) * (1 - gamma * gamma));

    double x = (-1. * plus_z.X() + fact1 * t.Vect().Unit().X()) / fact2;
    double y = (-1. * plus_z.Y() + fact1 * t.Vect().Unit().Y()) / fact2;
    double z = (-1. * plus_z.Z() + fact1 * t.Vect().Unit().Z()) / fact2;

    TVector3 basis_d(x, y, z);

    //e = cos(lp.Vect().Unit().Angle(basis_d));
    //f = cos(lm.Vect().Unit().Angle(basis_d));

    e = lp.Vect().Unit().Dot(basis_d.Unit());
    f = lm.Vect().Unit().Dot(basis_d.Unit());
    offDiagD = lm.Vect().Unit().Dot(lp.Vect().Unit());

    //offDiagD = cos(lm.Vect().Unit().Angle(lp.Vect().Unit()));

    //LHC Optimised Basis

    //compute eigen vectors and values of C
    //different kappa to the one we normally use
    //for gluon process

    double alphas = 1. / 137.03599907;
    double beta = t.Beta();
    double pi = 3.1415926535897932384626;

    z = pdotk;

    double kappa = (pi * pi * alphas * alphas / 24.) * (7 + 9 * z * z * beta * beta) / (pow(1 - z * z * beta * beta, 2));
    double c0 = -2 * kappa * (1 - 2 * beta * beta + 2 * (1 - z * z) * pow(beta, 4) + pow(z, 4) * pow(beta, 4));
    double c4 = 4 * kappa * (1 - z * z) * beta * beta;
    double c5 = 4 * kappa * beta * beta * (-2 * z * z * (1 - z * z) * sqrt(1 - beta * beta) + 2 * (z * z + beta * beta)*(1 - z * z) - 1 + beta * beta * pow(z, 4));
    double c6 = -4 * kappa * z * (1 - z * z) * beta * beta * (1 - sqrt(1 - beta * beta));

    //    std::cout << "kg: " << kappa << std::endl;
    //    std::cout << "beta: " << beta << std::endl;
    //    std::cout << "kg: " << kappa << std::endl;
    //    std::cout << "c0: " << c0 << std::endl;
    //    std::cout << "c4: " << c4 << std::endl;
    //    std::cout << "c5: " << c5 << std::endl;
    //    std::cout << "c6: " << c6 << std::endl;

    //eigen parts
    double part1 = c0 + 0.5 * c4 + 0.5 * c5 + z * c6;
    double part2 = 0.5 * sqrt(c5 * c5
            + c4 * c4
            + 4 * c6 * c6
            - 2 * c4 * c5
            + 4 * z * c5 * c6
            + 4 * z * c4 * c6
            + 4 * z * z * c5 * c4);

    //std::cout << "part1: " << part1 << std::endl;
    //std::cout << "part2: " << part2 << std::endl;

    //get eigen values
    double eigen1 = c0;
    double eigen2 = part1 + part2;
    double eigen3 = part1 - part2;

    //get correct eigen vector

    //std::cout << "Eigen Values" << std::endl;
    //std::cout << eigen1 << std::endl;
    //std::cout << eigen2 << std::endl;
    //std::cout << eigen3 << std::endl;

    TVector3 eigenvec;

    double infrontofp1 = 0.5 * c4 - 0.5 * c5;
    double infrontofp2 = 0.5 * sqrt(
            c5 * c5
            + c4 * c4
            + 4 * c6 * c6
            - 2 * c4 * c5
            + 4 * z * c5 * c6
            + 4 * z * c4 * c6
            + 4 * z * z * c5 * c4);

    double infrontofk = z * c5 + c6;

    TVector3 kvec = t.Vect().Unit(); //(sqrt(1 - z * z), 0, z);

    if (fabs(eigen1) > fabs(eigen2) && fabs(eigen1) > fabs(eigen3)) {
        //std::cout << "Eigen Value 1 has largest magnitude" << std::endl;

        eigenvec = plus_z.Cross(kvec.Unit()).Unit();

        if (eigen1 < 0.) {
            //g = cos(lp.Vect().Unit().Angle(-1 * eigenvec));
            //h = cos(lm.Vect().Unit().Angle(eigenvec));
            g = lp.Vect().Unit().Dot(-1 * eigenvec.Unit());
            h = lm.Vect().Unit().Dot(eigenvec.Unit());
        } else {
            //g = cos(lp.Vect().Unit().Angle(eigenvec));
            //h = cos(lm.Vect().Unit().Angle(eigenvec));
            g = lp.Vect().Unit().Dot(eigenvec.Unit());
            h = lm.Vect().Unit().Dot(eigenvec.Unit());
        }

    } else if (fabs(eigen2) > fabs(eigen1) && fabs(eigen2) > fabs(eigen3)) {
        //std::cout << "Eigen Value 2 has largest magnitude" << std::endl;

        eigenvec = ((infrontofp1 + infrontofp2) * plus_z + infrontofk * kvec.Unit()).Unit();

        if (eigen2 < 0.) {
            //g = cos(lp.Vect().Unit().Angle(-1 * eigenvec));
            //h = cos(lm.Vect().Unit().Angle(eigenvec));
            g = lp.Vect().Unit().Dot(-1 * eigenvec.Unit());
            h = lm.Vect().Unit().Dot(eigenvec.Unit());
        } else {
            //g = cos(lp.Vect().Unit().Angle(eigenvec));
            //h = cos(lm.Vect().Unit().Angle(eigenvec));
            g = lp.Vect().Unit().Dot(eigenvec.Unit());
            h = lm.Vect().Unit().Dot(eigenvec.Unit());
        }

    } else if (fabs(eigen3) > fabs(eigen1) && fabs(eigen3) > fabs(eigen2)) {
        //std::cout << "Eigen Value 3 has largest magnitude" << std::endl;

        eigenvec = ((infrontofp1 - infrontofp2) * plus_z + infrontofk * kvec.Unit()).Unit();

        if (eigen3 < 0.) {
            //g = cos(lp.Vect().Unit().Angle(-1 * eigenvec));
            //h = cos(lm.Vect().Unit().Angle(eigenvec));
            g = lp.Vect().Unit().Dot(-1 * eigenvec.Unit());
            h = lm.Vect().Unit().Dot(eigenvec.Unit());
        } else {
            //g = cos(lp.Vect().Unit().Angle(eigenvec));
            //h = cos(lm.Vect().Unit().Angle(eigenvec));
            g = lp.Vect().Unit().Dot(eigenvec.Unit());
            h = lm.Vect().Unit().Dot(eigenvec.Unit());
        }
    }

    //optD = cos(lm.Vect().Unit().Angle(lp.Vect().Unit()));
    optD = lm.Vect().Unit().Dot(lp.Vect().Unit());
}

/**
 * Calculate the W polarisation angle as described in the Top Properties CSC note, pg12.
 *
 * Defined as the angle between the W direction in the top quark rest frame and the charged
 * lepton direction in the W rest frame after a boost in the W flying direction.
 *
 * @param top    The reconstructed or matrix element top quark.
 * @param w      The W boson that is a daughter of the top quark.
 * @param lepton The lepton that belongs to the W that belongs to the top quark.
 *
 * @return cos(psi).
 */
double MatrixElementAnalysis::wPolarisation(TLorentzVector top, TLorentzVector w, TLorentzVector lepton) {
    //cout << "w polarisation " << endl;
    //cout << "T  " << top << endl;
    //cout << "W  " << w << endl;
    //cout << "l  " << lepton << endl;

    TLorentzVector w2(w);

    //cout << "w2 " << w2 << endl;

    //W direction in the top rest frame
    TVector3 boost_to_t = -1. * top.BoostVector();
    w.Boost(boost_to_t);
    TVector3 wDirection = w.Vect().Unit();

    //cout << wDirection.X() << " " << wDirection.Y() << " " << wDirection.Z() << endl;

    //lepton direction in W rest frame.
    TVector3 boost_to_w = -1. * w.BoostVector();
    lepton.Boost(boost_to_w);
    TVector3 leptonDirection =  lepton.Vect().Unit();

    //cout << leptonDirection.X() << " " << leptonDirection.Y() << " " << leptonDirection.Z() << endl;

    double p = wDirection.Dot(leptonDirection);

    //cout << "Returning " << p << endl;
    return p;
}
