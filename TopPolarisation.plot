# BEGIN PLOT /ATLAS_2011_I915920/Cuts_lowpt
Title=Number of events passing low $p_{T}$ selections (0 = all events)
XLabel=Cut
YLabel=$N_{\mathrm{pass}}$
LogY=0
RatioPlot=0
Legend=0
# END PLOT
