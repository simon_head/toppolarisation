## Makefile to build and install Rivet analyses

CC=g++
WFLAGS= -Wall -Wextra
CFLAGS=-m64 -fPIC -pg -I$(INCDIR) -I$(RIVETINCDIR) -O3 -march=native $(WFLAGS) -pedantic -ansi
ATOMCFLAGS=-std=c++11 -m64 -pg -O2 $(WFLAGS) -pedantic -ansi -shared -fPIC

INCDIR=-I$(PWD)/include -I$(shell root-config --cflags)
LIBDIR:=-L$(shell rivet-config --libdir) -L$(shell root-config --libdir --libs)
PREFIX:=$(shell rivet-config --prefix)
RIVETINCDIR:=$(shell rivet-config --includedir)
LDFLAGS:=$(shell rivet-config --ldflags)

ATOMINCDIR=$(shell atom-config --cppflags) $(shell root-config --cflags) -I$(PWD)/include
ATOMLDFLAGS=$(shell atom-config --ldflags)  -lAtomCore -lAtomBase -lAtomData -lAtomDetAtlas -lAtomDetCms -lAtomHepMC_IO -lAtomLHE_IO -lAtomProjections -lAtomSimulation -lAtomStdHep_IO -lAtomTools -lAtomWeight_IO -lRivetBase -lRivetProjections -lRivetTools -lfastjet -lfastjettools -lfastjetplugins $(shell root-config --libs)


## Get the first part of the string passed
ANALYSISPATH = $(firstword $(subst :, ,$1))

all: MatrixElement.o RivetTopPolarisation.so
RivetTopPolarisation.so: src/TopPolarisation.cc MatrixElement.o
	rivet-buildplugin RivetTopPolarisation.so src/TopPolarisation.cc $(INCDIR) $(LIBDIR) MatrixElement.o

MatrixElement.o: src/MatrixElementAnalysis.cxx include/MatrixElementAnalysis.h
	${CC} ${CFLAGS} -o $@ -c $< -I${INCDIR} -lm ${INCDIR} ${LIBDIR}

atom: libAtom_TopPolarisation.so

libAtom_TopPolarisation.so: src/ATOM_TopPolarisation.cc MatrixElement.o
	$(CC) $(ATOMCFLAGS) $(ATOMINCDIR) -o "libAtom_TopPolarisation.so" src/ATOM_TopPolarisation.cc $(ATOMLDFLAGS) MatrixElement.o
	cp -f libAtom_TopPolarisation.so $(call ANALYSISPATH, $(ATOM_ANALYSIS_PATH))

install: RivetTopPolarisation.so
	cp -f RivetTopPolarisation.so $(call ANALYSISPATH, $(RIVET_ANALYSIS_PATH))
clean:
	rm -f *.o  *.so
